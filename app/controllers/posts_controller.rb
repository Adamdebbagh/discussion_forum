class PostsController < ApplicationController
  before_filter :authenticate_user!, except: [:show, :index, :articles]
  before_filter :find_post, only: [:show, :edit, :update, :destroy, :upvote, :downvote]
  impressionist :actions=>[:show,:index]

  def index
    #@posts = Post.order('created_at DESC').page(params[:page]).per(3)
    if params[:category].blank?
      @posts = Post.all.order('created_at DESC').page(params[:page]).per(10)
    else
      @category_id = Category.find_by(name: params[:category]).id
      @posts = Post.where(category_id: @category_id).order('created_at DESC').page(params[:page]).per(10)
    end

  end
  def show

    if request.path != post_path(@post)
      redirect_to @post, :status => :moved_permanently
    end

    @comments = Comment.where(post_id: @post.id).order('created_at DESC')
    @related_articles = Post.tagged_with(@post.tag_list, any: true).order('created_at DESC').page(params[:page]).per(2)
    impressionist(@post)

    @user = @post.user
    @latest_posts = Post.where(user_id: @user.id).order('created_at DESC').limit(5)
  end
  def new
    @post = Post.new
    #@categories = Category.all.map{|c| [ c.name, c.id ] }
  end
  def create
    @user = current_user
    @post = @user.posts.build(allowed_params)
   # @post.category_id = params[:category_id]

    if @post.save
      flash[:success] = 'New Post Created'
      redirect_to @post
    else
      render 'new'
    end
  end
  def edit
    redirect_to posts_path unless @post.user == current_user
   # @categories = Category.all.map{|c| [ c.name, c.id ] }
  end
  def update
    if @post.user == current_user
      if @post.update_attributes(allowed_params)
        flash[:success] = 'Updated post'
        redirect_to @post
      else
        render 'edit'
      end
    else
      redirect_to posts_path
      flash[:notice] = 'Action Failed!'
    end
    #@post.category_id = params[:category_id]
  end
  def destroy
    if @post.user == current_user
      @post.destroy
      redirect_to posts_path
    else
      redirect_to root_path
      flash[:danger] = 'Action Failed!'
    end
  end

  def upvote
    @post.upvote_by current_user
    redirect_to :back
  end

  def downvote
    @post.downvote_by current_user
    redirect_to :back
  end

  def tags
    if params[:tag].present?
      @posts = Post.tagged_with(params[:tag])
    else
      @posts = Post.all
    end
  end
    def articles
      @user = User.find(params[:author])
    if params[:author].present?
      @articles = Post.where(user_id: @user).order('created_at DESC').page(params[:page]).per(10)
    end
  end

  private
  def allowed_params
    params.require(:post).permit(:image, :caption, :title, :excerpt, :body, :category_id, :tag_list, :author)
  end
  def find_post
    @post = Post.friendly.find(params[:id])
  end
end
