class UsersController < ApplicationController
  before_filter :authenticate_user!, except: [:show, :index, :articles]
  def index
    @users = User.page(params[:page]).per(10)
  end
  def show
    @user = User.friendly.find(params[:id])
  end
end
