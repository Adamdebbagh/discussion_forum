class CommentsController < ApplicationController
  before_filter :authenticate_user!
  def create
    @post = Post.friendly.find(params[:post_id])
    @comment = @post.comments.create(params[:comment].permit(:name, :body))
    @comment.user_id = current_user.id if current_user
    #@comment.post_id = @post.id
    @comment.save
    redirect_to post_path(@post)
  end
  def edit
    @post = Post.friendly.find(params[:post_id])
    @comment = @post.comments.find(params[:id])
  end

  def update
    @post = Post.friendly.find(params[:post_id])
    @comment = @post.comments.find(params[:id])

    if @comment.update(params[:comment].permit(:body))
      redirect_to post_path(@post)
    else
      render 'edit'
    end
  end
  def destroy
    @post = Post.friendly.find(params[:post_id])
    @comment = @post.comments.find(params[:id])
    @comment.destroy
    redirect_to post_path(@post)
  end
end
