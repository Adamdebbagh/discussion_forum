class Post < ActiveRecord::Base

  extend FriendlyId
  acts_as_votable
  acts_as_taggable_on :tags
  is_impressionable


  validates :title, presence: true, length: { minimum: 3 }
  validates :body, presence: true, length: { minimum: 250 }
  validates :category, presence: true
  validates :tag_list, presence: true

  friendly_id :title,use: [:slugged, :history]

  def should_generate_new_friendly_id?
    title_changed? ||new_record? || slug.nil? || slug.blank?
  end


  has_attached_file :image, :styles => { :large => '900x300>',
                                         :medium => '300x300#',
                                         :square => '200x200#',
                                         :thumb => '100x100>' ,
                    :default_url => "/images/:style/missing.png"}

  validates_attachment_content_type :image,
                                    :content_type => /\Aimage\/.*\Z/
  belongs_to :user
  has_many :comments
  belongs_to :category



  def score
    self.get_upvotes.size - self.get_downvotes.size
  end


end

