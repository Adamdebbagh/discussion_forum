class User < ActiveRecord::Base

  extend FriendlyId
  # Include default users modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable


  friendly_id :name, use: :slugged
  def should_generate_new_friendly_id?
    name_changed? ||new_record? || slug.nil? || slug.blank?
  end

    has_attached_file :avatar, :styles => { :medium => "300x300#",
                                          :square => '200x200#',
                                          :thumb => "100x100>" },
                    :default_url => "/images/:style/missing.png"
  validates_attachment_content_type :avatar,
                                    :content_type => /\Aimage\/.*\Z/

  def country_name
    country = ISO3166::Country[country_code]
    country.translations[I18n.locale.to_s] || country.name
  end

  has_many :posts, dependent: :destroy

  has_many :comments, dependent: :destroy
  has_many :sent_invites, class_name: "Relationship", foreign_key: :inviting_id
  has_many :received_invites, class_name: "Relationship", foreign_key: :invited_id

  has_many :invited_users, through: :sent_invites, source: :invited_user
  has_many :inviting_users, through: :received_invites, source: :inviting_user

  def full_name
    if self.name.blank?
      self.email
    else
      self.name.titleize
    end
  end

end
