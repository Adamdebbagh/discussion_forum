# Root crumb
crumb :root do
  link "Home", root_path
end
crumb :post do |p|

  link p.title, p
  parent :root
end
crumb :user do |u|

    link 'Profile',users_path(u)

end
crumb :new_post do |p|

  link  "New", p

end
crumb :edit_post do |post|
  link "Edit #{post.title}", post
  parent :post, post
end
crumb :edit_profile do
  link "Edit Profile"
  parent :root
end

=begin
# Issue list
crumb :posts do
  link "All posts", issues_path
end

# Issue
crumb :post do |post|
  link post.title, post
  parent :post
end
=end

# crumb :projects do
#   link "Projects", projects_path
# end

# crumb :project do |project|
#   link project.name, project_path(project)
#   parent :projects
# end

# crumb :project_issues do |project|
#   link "Issues", project_issues_path(project)
#   parent :project, project
# end

# crumb :issue do |issue|
#   link issue.title, issue_path(issue)
#   parent :project_issues, issue.project
# end

# If you want to split your breadcrumbs configuration over multiple files, you
# can create a folder named `config/breadcrumbs` and put your configuration
# files there. All *.rb files (e.g. `frontend.rb` or `products.rb`) in that
# folder are loaded and reloaded automatically when you change them, just like
# this file (`config/breadcrumbs.rb`).